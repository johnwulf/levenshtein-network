require 'optparse'



def read_file(name, word)
  v = []
  File.foreach(name) do |w|
    v << w unless w.eql?(word)
  end
  
  return v
end

def write_file(name, values)
  File.open(name, 'w') do |file|
    values.sort!.each do |m|
      file.puts m
    end
  end
end

def add_words(list, population, s)
  while(list.size < s)
    list << population.delete_at(Random.rand(population.size))
  end
end

options = {}
optparse = OptionParser.new do |opts|
  opts.banner= "Usage: causes.rb [options]"

  options[:word] = "words.txt"
  opts.on('-w', '--word WORD', "Use WORD as the root of the social network.") do |word|
    options[:word] = word
  end

  options[:file] = "in.txt"
  opts.on('-i', '--in IN', "Use the words in IN as the word space. IN is " +
      "assumed to contain one word per line.") do |file|
    options[:file] = file
  end
  
  opts.on('-h', '--help', 'Display this screen.') do
    puts opts
    exit
  end
  
end
optparse.parse!

word = options[:word]
file = options[:file]


population = read_file(file, word)
puts "Creating test file for word '#{word}' from initial population #{file} with size #{population.size}"

list = []
s = 10000
size_list = []

puts "Creating population sizes"
while (s + 10000) <  population.size
  s += 10000
end

while(s > 10000)
  size_list.insert(0, s)
  s /= 2 
end


puts size_list
list << word

size_list.each do |size|  
  puts "creating list with members #{size}"
  add_words(list, population, size)
  write_file(file + ".#{size}" , list)
  puts "Created #{file}.#{size}"
end

