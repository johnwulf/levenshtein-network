class Network
  attr_reader :members
  attr_accessor :candidates
  attr_reader :min_level
  attr_reader :id
  
  def initialize(id, first_member)
    @members = { first_member => true }
    @id = id
    
    @candidates = {}
    #Pick a large number
    @min_level = first_member.size
    add_candidates(first_member)
  end
  
  def add_member(member)
    if member.size < @min_level
      # we are at a new level, so need to clear the candidates
      @candidates.clear  
      @min_level = member.size
    end
    
    # Now we add the member
    @members[member] = true
    
    # And add the candidates
    add_candidates(member)    
  end
  
  # TODO: this makes an assumption that the min_level is either greater
  #       or equal to the level given., which works for what we are using 
  #       the network class, but not in general
  def merge_for_level(level, other_network)

    # merge members
    @members.update(other_network.members)

    # Figure out how to handle the candidates
    # Fix the situation where one min_level < level => error in our case
    if other_network.min_level == level
      if @min_level == level
        # both networks are on the same level
        @candidates.update(other_network.candidates)
      else
        # we just get the array from the other network, as we don't need it 
        # anymore there. In order to avoid a problem, we initialize the 
        # other networks candidates with an empty list
        @candidates = other_network.candidates
        other_network.candidates = {}
        @min_level = other_network.min_level
      end
    end
  end
  
  def merge_candidates(candidates) 
    @candidates.update(candidates)
  end
  
  def add_candidates(member)
    (0...member.size).each do |n|
      candidate = member.dup
      candidate.slice!(n)
      
      # We add a character in front of the string to indicate
      # which character was sliced. This prevents the following
      # situation:
      # Assume the two members 'acsomething' and 'casomething'
      # This would create the following candidates
      #   asomething, csomething 
      # by eleminating position 1 from one member and position 2 from the other
      # and vice versa
      # Generally, this is good when looking for candidates in level - 1
      # However, to see if acsomething and casomething are related this would fail
      # it looks like it, but you need two character changes. 
      # We fix that by prepending a character that depends on the position
      # that was changed. The creates
      #  Basomething, Aasomething, Bcsomething, Acsomething
      # Now the information with position was eliminated is contained in the 
      # candidate and acsomething and casomething no longer come up as related
      # When looking for the next level, we simply strip that first character.
      # This will lead to some duplication, but it shouldn't be too bad.
      # TODO: can we do this better?
      candidate = (65 + n).chr + candidate
      @candidates[candidate] = true
    end
  end
  
  def is_member?(member) 
    members.has_key?(member)
  end
  
  def size 
    members.size
  end
end
