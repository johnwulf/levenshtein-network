require 'optparse'
require './population.rb'
require './network.rb'

options = {}
optparse = OptionParser.new do |opts|
  opts.banner= "Usage: causes.rb [options]"

  options[:word] = "causes"
  opts.on('-w', '--word WORD', "Use WORD as the root of the social network.") do |word|
    options[:word] = word
  end

  options[:in] = "words.txt"
  opts.on('-i', '--in IN', "Use the words in IN as the word space. IN is " +
      "assumed to contain one word per line.") do |file|
    options[:in] = file
  end
  
  options[:out] = "result.txt"
   opts.on('-o', '--out OUT', "Write the network to OUT.") do |file|
     options[:out] = file
   end

  opts.on('-h', '--help', 'Display this screen.') do
    puts opts
    exit
  end
end
optparse.parse!

root_word = options[:word]
word_file = options[:in]
out_file = options[:out]

population = Population.new

puts ("Loading population from #{word_file}")
File.foreach(word_file) do |word|
  population.add_member(word.chomp)
end

beginning_time = Time.now

puts ("Building networks for population")
population.build_networks
puts ("Retrieving network for '#{root_word}'")

network = population.get_network(root_word)
puts ("The word is in network with id #{network.id}")
puts("The size of  #{root_word}'s network is: #{network.size}")
puts("Time elapsed #{(Time.now - beginning_time)}s")

puts("Writing result to #{out_file}")



File.open(out_file, 'w') do |file|
  network.members.keys.sort!.each do |m|
    file.puts m
  end
end

puts "Done."