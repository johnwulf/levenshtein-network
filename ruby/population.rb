require './network.rb'

class Population
  
  def initialize
    @population = []
    @networks = []
    @candidates = {}
    @new_network_candidates = {}
    @mergers = {}
  end

  def add_member(member)
    members = @population[member.size]
    
    if(members == nil)
      members = {}
      @population[member.size] = members
    end
    
    members[member] = false
  end
  
  def is_member?(member) 
    members = @population[member.size]
    
    return false if members == nil
    return members.has_key?(member)
  end
  
  def get_network(member)
    member_networks = []
    member_networks = member_networks + @networks.select do |network| 
      network && (network.is_member?(member)) 
    end .flatten
    
    puts "Oh my ... more than one network: #{member_networks.size}" if member_networks.size > 1
    return member_networks.first
  end
  
  def build_networks
    # This is the big method
    # We start with no networks and the entire population available
    @networks.clear
    @population.each do |members| 
      if members != nil 
        members.each_key { |k| members[k] = false }
      end
    end
    
    # Start at the level with the longest words
    @level = @population.size
    while(@level > 0)
      puts "Start processing level #{@level}" 
      puts "population size = #{@population[@level].size if @population[@level]}"
      puts "networks = #{@networks.select{|n| n}.size} with arrays size of #{@networks.size}"
      #
      # Step 1: Convert all members of the current level into networks, 
      #         if they not already part of a network
      #
      
      puts "Create networks for members on level without a network"
      make_networks_for_level
      
      #
      # Step 2: Connect all networks on the current level that are 
      #         related
      #
      
      puts "Connecting merge all networks that are related through this level"
      connect_networks_for_level
      
      # 
      # Step 3: Find new members in the next level
      #
      puts "Create connections of existing networks to next level"
      #find_new_members
      
      #
      # Step 4: Process the mergers
      #
      puts "Merging all the networks"
      merge_networks
      
      # Process the next level
      @level = @level - 1
    end
  end
  
  #
  # This method is quite simple. We processed the networks on the current level
  # which merged all related networks on that level and gave as a list of pairs
  # of candidates and networks that are all possible links into the next level.
  # Depending on which list is smaller, the population on the next level or the
  # list of candidates, we will iterate over one or the other to find matches.
  #
  def find_new_members

    members = @population[@level - 1]    
    # There have to be members in the next and candidates for the next level
    # If not, the networks from the upper level do not extend beyond current 
    # level. 
    if (members != nil) && (!@candidates.empty?)
      # This is really just a minor performance improvement. Iterate 
      # over the smaller set and use the other for lookup.
      # This doesn't seem optimal, b/c iterating over the candidates 
      # requires another lookup. However, the cost of the extra 
      # lookup is only paid if there is a connection. As we assume
      # that the connections are much smaller than any size, this
      # should work fine on average.
      #if @candidates.size > members.size
      #else
      #end
      # We have to use the candidates, b/c we need to 
      # strip the first character
      #find_new_members_through_candidates(members)
      find_new_members_through_population(members)
      
    end
  end
  
  def find_new_members_through_candidates(members)
    @candidates.each_key do |candidate|
      # this either returns 
      #  true if the member is already part of a network
      #  nil if the member doesn't exist in the population
      #  false if the member is not part of a network
      # TODO: swap true and false to make this simpler
      new_member = members[candidate]
      if (new_member != nil) and !new_member
        # we found a connection, so add the new member to the network
        @candidates[candidate].add_member(candidate)
        
        # and mark the member in the population as part of a network
        members[candidate] = true
      end
    end
  end
  
  def find_new_members_through_population(members)
    members.each_key do |member|
      network = @candidates[member]
      if network != nil
        # we found a connection, so add the new member to the network
        network.add_member(member)
        
        # and mark the member in the population as part of a network
        members[member] = true
      end
    end
  end
  
  #
  # This method is core. The goal is to keep the effort linear to the population size
  # p(l) of the level and not make it dependent on the networks. 
  # The order of this implemenation should be O(l * p(l)).
  # The idea is to take advantage of l - 1 (deletions) to find related networks. 
  # Two words on level l have the distance 1 if there is a word on level l - 1 
  # that has the distance 1 from both words. 
  # However, the population doesn't have to have all possible words on l - 1, so
  # an artificial level l - 1 is created that contains all words that could be created 
  # from all words on level l. This means in the worst case, the artifical level l - 1
  # is equal to the size of the population in level l time l.
  # Example:
  # bones -> ones bnes boes bons bone
  # tones -> ones tnes toes tons tone
  # zonis -> onis znis zois zons zoni
  #
  # "bones" and "tones" have "ones" in level l - 1 => their distance on level l must be 1
  # on the other hand "zonis" has nothing in common (don't ask)
  #
  # Unfortunately, a simple match is not sufficient, as we can think of the relation in 
  # a level a linked list. Looking only at two sets, we could look at to elements that are
  # connected via a third one. Take the following example:
  # bones -> ones bnes boes bons bone
  # tones -> ones tnes toes tons tone
  # tonis -> onis tnis tois tons toni
  # 
  # if we would happen to look at "bones" and "tones" first, we would not find that the the
  # two words are related, but they are within the same network through the connection of 
  # tones. So, in order to not increase the order, we use some data trickery that allows
  # us to process every network (word) only ones, thus keeping the effort linear.
  # In short, networks have pointers to words related words in level l - 1, which in turn
  # have pointers to their respective networks. That allows to touch every network and every 
  # word only once, thus keeping a linear order.
  # Example
  # network:bones -> ones bnes boes bons bone
  # network:tones -> ones tnes toes tons tone
  # network:tonis -> onis tnis tois tons toni
  # 
  # word(l-1):ones -> bones, tones  ** this is the node that connects bones and tones
  # word(l-1):bnes -> bones
  # word(l-1):boes -> bones
  # word(l-1):bons -> bones
  # word(l-1):bone -> bones
  # word(l-1):tnes -> tones
  # word(l-1):toes -> tones
  # word(l-1):tons -> tones, tonis ** this is the node that connects tonis and tones
  # word(l-1):tone -> tones
  # word(l-1):onis -> tonis  
  # word(l-1):toni -> tonis  
  # word(l-1):tnis -> tonis
  # word(l-1):tois -> tonis
  # word(l-1):toni -> tonis
  #
  #  TODO: update this. We don't maintain the lists as above, we simply collect mergers
  #        and keep all the networks alive, until we find the next level connections              
  def connect_networks_for_level
    netorks_to_delete = []
    networks_on_level = 0
    mo_count = 0
    merger_count = 0
    candidates_processed = 0
    new_members = []
    
    
    
    @candidates.clear
    members = @population[@level - 1]     
      
    @networks.each do |network|
      # Only process the network if it's min level is equal 
      # to our current level
      if network && (@level == network.min_level)
        # little bit of book keeping
        networks_on_level = networks_on_level + 1
        @new_network_candidates.clear
        new_members.clear
        
        #puts "Processing candiates for network #{network.id}"
        network.candidates.each_key do |candidate|
          candidate_next_level = candidate.slice(1...candidate.size)
                
          candidates_processed = candidates_processed + 2
          
          # Get the networks that have the candidate for this level
          other_network = @candidates[candidate]
          if other_network == nil
            # first network, just put it into the candidates list
            @candidates[candidate] = network
          elsif other_network != network
            merger_count = merger_count + 1
            # now it gets interesting we actually found two networks
            # that are related on the current level
            # We don't merge the networks here, as we first need to
            # find the connections in the next level
            # all we do is add them to the merger list
            add_merger(network, other_network)                      
          end
          
          #puts "population for next level is #{members}"
          # Now see if we have another level
          if (members != nil) 
            #puts "There is a population #{members}"
            # Let's see if the candidate without the leading character exists
            new_member = members[candidate_next_level]
            #puts "The new member value #{new_member} and the candiate is #{candidate_next_level}"          
            if (new_member != nil) and !new_member
              # there is a member that is not part of any other network
              # so let's add it to this network
              new_members << candidate_next_level
              @candidates[candidate_next_level] = network

              # and mark the member in the population as part of a network
              members[candidate_next_level] = true     
            elsif new_member != nil
              # so there is a member and it has been added to a network
              # let's propose a merger
              other_network = @candidates[candidate_next_level]
              merger_count = merger_count + 1
              add_merger(network, other_network)
            end
          end 
        end
        new_members.each {|new_member| network.add_member(new_member) }
      end
    end
    
    puts "level #{@level} stats: "
    puts "****   networks processed: #{networks_on_level}"
    puts "**** number of candidates: #{@candidates.size}"
    puts "**** candidates processed: #{candidates_processed}"
  end
  
  def merge_networks
    merger_count = 0
    merger_ops = 0
    other_networks = []
    merger_parties = {}
    new_merger_parties = {}

    while(!@mergers.empty?)
      #TODO: This should work as recursion too, which will make the code simpler
      #      I follow the links until I hit a node and then walk back up
      #      The maximum depth should be the length of strings, just need avoid circular 
      #      recursions
      
      # Step 1: Collect merger parties for first network in list
      merger_parties.clear
      new_merger_parties.clear # Should be empty anyway!!!
      merger = @mergers.first
      #puts "----------------------------------------------------"
      #puts "++++merger #{merger}"
      # initialize the new_merger_parties with one network
      new_merger_parties[merger[0]] = @networks[merger[0]] # There should be a better way => create hash method for network and use network directly
      
      # This walks the list
      while(!new_merger_parties.empty?)
        #puts "++++new_parties #{new_merger_parties}"

        # get the next network from the hash (it is the second value of the hash entry)
        network = new_merger_parties.shift[1]
        
        # add it to the merger parties
        merger_parties[network.id] = network
        #puts "++++mergers #{@mergers}"
        #puts "++++network #{network.id}"
        #puts "++++merger_parties #{merger_parties}"
        other_networks = @mergers.delete(network.id)
        if other_networks
          other_networks.each do |other_network|
            # new merger parties are parties that are not already in the merger parties list
            # i.e. we have not visited that node in the link list yet
            new_merger_parties[other_network.id] = other_network unless merger_parties[other_network.id]
            merger_ops += 1
          end
        end
      end  
      
      # Step 2: Merger time
      # We spend a little effort here to reduce computing time
      # Start with picking the first off the list (arbitrary)
      network = merger_parties.shift[1]
      merger_parties.each_value do |other_network|
        #puts "++++ merge network #{network.id} with #{other_network.id}"        
        network = merge_parties(network, other_network)
        merger_count += 1
      end      
    end    
    puts "****    mergers performed: #{merger_count}"
    puts "****    merger operations: #{merger_ops}"
  end
  
  def merge_parties(network_a, network_b)
    # Merge the smaller into the larger network
    if network_a.size >= network_b.size
      # We first need to figure out what to do with the
      # candidates
      network_a.merge_for_level(@level - 1, network_b)
      # The network b ceased to exist => remove it from the networks array 
      @networks[network_b.id] = nil
      
      # and we return the network that is lev
      merged_network = network_a      
    else
      # just call yourself with switched networks
      merged_network = merge_parties(network_b, network_a)
    end    
    
    return merged_network
  end
  
  def add_merger (network_a, network_b)
    #connect the two parties through a merger (double linked list)
    add_merger_party(network_a, network_b)
    add_merger_party(network_b, network_a)
  end
  
  def add_merger_party (network_a, network_b)
    merger_parties = @mergers[network_a.id]
    
    # we use a simple array for the merger list, as two networks
    # can only have one connection point ever if they have a 
    # Levenshtein distance of 1
    if merger_parties == nil
      merger_parties = []
      @mergers[network_a.id] = merger_parties
    end
    
    merger_parties << network_b
  end
  
  def make_networks_for_level
    members = @population[@level]
    if members != nil
      members.select { |k| !members[k] }.keys.each do |m|
        network = Network.new(@networks.size, m)
          @networks[@networks.size] = network 
          members[m] = true
      end
    end
  end  
end
