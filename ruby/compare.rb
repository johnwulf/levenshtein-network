require 'optparse'



def read_file(name)
  v = {}
  File.foreach(name) do |word|
    v[word.chomp] = true
  end
  
  return v
end

def write_file(name, values)
  File.open(name, 'w') do |file|
    values.sort!.each do |m|
      file.puts m
    end
  end
end

options = {}
optparse = OptionParser.new do |opts|
  opts.banner= "Usage: causes.rb [options]"

  options[:file_1] = "causes"
  opts.on('-a', '--afile FILE', "Use WORD as the root of the social network.") do |file|
    options[:file_1] = file
  end

  options[:file_2] = "words.txt"
  opts.on('-b', '--bfile FILE', "Use the words in IN as the word space. IN is " +
      "assumed to contain one word per line.") do |file|
    options[:file_2] = file
  end
  
  options[:out] = "result.txt"
   opts.on('-o', '--out OUT', "Write the network to OUT.") do |file|
     options[:out] = file
   end

  opts.on('-h', '--help', 'Display this screen.') do
    puts opts
    exit
  end
  
end
optparse.parse!

a = options[:file_1]
b = options[:file_2]
out_file = options[:out]

puts "comparing '#{a}' to '#{b}'. Writing diff to '#{out_file + ".diff"}' and intersection to '#{out_file}'"

va = read_file(a)
vb = read_file(b)

puts "#{a}: #{va.size}"
puts "#{b}: #{vb.size}"

only_a = []
only_b = []
common = []

va.each_key do |k|
  if vb[k] != nil
    common << k
    vb.delete(k)
  else
    only_a << k
  end
end

only_b = vb.keys

puts "Common: #{common.size}"
puts "Only in #{a}: #{only_a.size}"
puts "Only in #{b}: #{only_b.size}"

write_file(a + ".only", only_a)
write_file(b + ".only", only_b)
write_file(out_file, common)

