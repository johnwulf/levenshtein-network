package causes;


public class Member {
  public String name;
  public int state;
  public int id;

  public Member(String name, int id) {
    this.name = name;
    this.id = id;
    this.state = -1;
  }

  public boolean equals(Object o) {
    if(o instanceof Member) {
      return name.equals(((Member)o).name);
    }

    return false;
  }

  public String toString() {
    StringBuffer b = new StringBuffer();

    b.append("{name:").append(name).append(", id:").append(id).append(", state:").append(state).append('}');

    return b.toString();
  }
}
