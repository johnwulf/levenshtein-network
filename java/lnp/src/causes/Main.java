package causes;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class Main {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
	  long millis = System.currentTimeMillis();
	  
	  if(args.length == 3) {
  	  int index = 0;
  	  System.out.println("Building the population:" + args[1]);
  	  Population p = new Population(args[1]);
  	  
  	  System.out.println("Initializing the population");
  	  p.initialize();
  	  System.out.println("Population size: " + p.size());
  	  
  	  Friendster f = new Friendster(p);
  	  System.out.println("Searching for '" + args[0] + "'");
  	  f.createNetwork(args[0].trim()); 
  	  
  	  System.out.println("Done ... THE SIZE OF THE NETWORK FOR " + f.getMember().name + " IS " + f.getNetwork().size());
  	  System.out.println("Duration: " + (System.currentTimeMillis() - millis));
  	  System.out.println("Writing result to:" + args[2]);
  	  
  	  BufferedWriter out = new BufferedWriter(new FileWriter(args[2]));
  	  ArrayList<Member> network = f.getNetwork();
  	  Collections.sort(network);
  	  for(Member m:network) {
  	    out.write(m.name);
  	    out.newLine();
  	  }
  	  out.close();
	  }
	  else {
	    System.out.print("usage: java causes.Main <word> <input file> <output file>");
	  }
	}
}
