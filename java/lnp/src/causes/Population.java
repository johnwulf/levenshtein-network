package causes;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class Population {  
  private ArrayList<Member> population;
  private HashMap<String, SyllableEntry> syllableTree;
  
  public Population(String filename) throws IOException {
    syllableTree = new HashMap<String, SyllableEntry>();
    population = new ArrayList<Member>();
    
    BufferedReader in = new BufferedReader(new FileReader(filename));
    String name;
    Member member;
    int i = 0;
    int dot = 0;
    
    System.out.print("Loading Population ...");
    name = in.readLine();
    while(name != null) {
      member = new Member(name.trim(), i);
      addMember(member);
      i++;
      name = in.readLine();
      if(dot >= 5000) {
        System.out.print('.');
        dot = 0;
      }
      dot++;
      
      //System.out.println("Processed " + name + " which is member number " + i + "; Size of syllables: " + syllableTree.size());
    }
    System.out.println("DONE");
    in.close();
  }
  
  public long size() {
    return population.size();
  }
	
  public void initialize() {
    for(Member m:population) {
      m.state = -1;
    }
  }
  
  public Member findMember(String name) {
    Member member = null;
    for(Member m:population) {
      if(m.name.equals(name)) {
        member = m;
        break;
      } 
    }
    
    return member;
  }
  
  public SyllableEntry getSyllableEntry(String syllable) {
    return syllableTree.get(syllable);
  }
  
  private void addMember(Member m) {
    char name[] = m.name.toCharArray();
    String begin, rest;
    SyllableEntry syllable;
    StringBuffer buffer = new StringBuffer();
    
    // Add the member to the population
    population.add(m);
    
    // Set the member on the syllable as "the" member
    syllable = getSyllable(m.name);
    syllable.setMember(m);

    
    // Now break down the member into it's syllables and add the member
    // to the appropriate syllables.
    buffer.append("[ " + m.name + " ");
    for(int i=1; i < name.length; i++) {
      begin = new String(name, 0, i);
      rest = new String(name, i, name.length - i);
      buffer.append("{" + begin + " | " + rest + "} ");
      syllable = getSyllable(begin);
      syllable.addBegin(m);
      syllable = getSyllable(rest);
      syllable.addRest(m);
    }
    buffer.append("]");
    // System.out.println(buffer);
  }
  
  private SyllableEntry getSyllable(String syllable) {
    SyllableEntry entry = syllableTree.get(syllable);
    if(entry == null) {
      entry = new SyllableEntry(syllable, null);
      syllableTree.put(syllable, entry);
    }
    
    return entry;
  }
}



