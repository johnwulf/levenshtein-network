package causes;

import java.util.ArrayList;
import java.util.HashMap;

public class Friendster {
  private Population p;
  private ArrayList<Member> network;
  private Member member;

  public Friendster(Population p) {
    this.p = p;
    network = new ArrayList<Member>();
    member = null;
  }
  
  public ArrayList<Member> getNetwork() {
    return network;
  }
  
  public Member getMember() {
    return member;
  }
  
  public void createNetwork(String name) {
    int index = 0;
    int dot = 0;
    initialize(name);
    System.out.print("Building network ...");
 
    while(network.size() > index) {
      processMember(network.get(index));     
      index++;
      
      if(dot == 3000) {
        System.out.print('.');
        dot = 0;
      }
      
      dot++;
      //System.out.println("member:" + network.get(index - 1) + ", network size:" + network.size() + ", next member index:" + index + ", queue:" + (network.size() - index));
    }
    System.out.println("DONE");
  }
  
  private void processMember(Member m) {
    String name;
    String begin;
    String rest;
    String lastBegin = "";
    StringBuffer b = new StringBuffer();

    name = m.name;
    
    for(int i=0; i <= name.length(); i++) {     
      begin = name.substring(0, i);
      rest = name.substring(i);
      
      b.append("{" + begin + " | " + rest + "} ");

      //
      // Find candidates with same length (substitution)
      //
      if(lastBegin.isEmpty() && (!begin.isEmpty())) {
        findRestMembers(rest, name.length());
      }
      else if(begin.length() == (name.length() - 1)) {
        findMembers(lastBegin, rest, name.length());
        findBeginMembers(begin, name.length());
      }
      else if(!(rest.isEmpty() || lastBegin.isEmpty())) {
        findMembers(lastBegin, rest, name.length());
      }
      
      // 
      // Find candidates with length + 1 (insert)
      //
      
      if(begin.isEmpty()) {
        findRestMembers(rest, name.length() + 1);
      }
      else if(rest.isEmpty()){
        findBeginMembers(begin, name.length() + 1);
      }
      else {
        findMembers(begin, rest, name.length() + 1);
      }
      
      // 
      // Find candidates with length - 1 (delete)
      //
      
      // We don't have a "begin" (first case)
      if(lastBegin.isEmpty() && (!begin.isEmpty())) { 
        findRestMembers(rest, name.length() - 1);
      }
      // We don't have a "rest" (last case)
      else if(begin.length() == (name.length() - 1)) {
        findMembers(lastBegin, rest, name.length() - 1);
        findBeginMembers(begin, name.length() - 1);
      }
      // Everything is normal here, as long as rest is not empty
      else if(!(rest.isEmpty() || lastBegin.isEmpty())) {        
        findMembers(lastBegin, rest, name.length() - 1);
      }
      
      lastBegin = begin;
    }    
    //System.out.println(b.toString());
  }
  
  private void findMembers(String begin, String rest, int len) {
    SyllableEntry syllableBegin, syllableRest;
    HashMap<String, Member> beginCandidates;
    ArrayList<Member> restCandidates;

    syllableBegin = p.getSyllableEntry(begin);
    beginCandidates = syllableBegin.getBeginMemberList(len);
    syllableRest = p.getSyllableEntry(rest);
    restCandidates = syllableRest.getRestMemberList(len);
  
 //   System.out.println("Candidates: begin=" + (beginCandidates != null ? beginCandidates.size(): "null") +
 //       ", rest=" + (restCandidates != null ? restCandidates.size() : "null"));
    processCandidates(beginCandidates, restCandidates);
  }
  
  private void findBeginMembers(String begin, int len) {
    SyllableEntry syllableBegin;
    HashMap<String, Member> beginCandidates;
    
    syllableBegin = p.getSyllableEntry(begin);
    beginCandidates = syllableBegin.getBeginMemberList(len);
    addAllCandidates(beginCandidates);
  }
  
  private void findRestMembers(String rest, int len) {
    SyllableEntry syllableRest;
    ArrayList<Member> restCandidates;

    syllableRest = p.getSyllableEntry(rest);
    restCandidates = syllableRest.getRestMemberList(len);
    addAllCandidates(restCandidates);   
  }
  
  
  private void processCandidates(HashMap<String, Member> beginCandidates, ArrayList<Member> restCandidates) {
    Member member;

    // Begin and end have to match
    if(beginCandidates != null) {
      if(restCandidates != null) {
        for(Member candidate:restCandidates) {
          if(candidate.state == -1) {
            member = beginCandidates.get(candidate.name);
            if(member != null) {
              member.state = 0;
              network.add(member);
            }
          }
        }
      }
    }
  }
  
  private void addAllCandidates(ArrayList<Member> candidates) {
    if(candidates != null) {
      for(Member candidate:candidates) {
        if(candidate.state == -1) {
          candidate.state = 0;
          network.add(candidate);
        }
      }
    }
  }
  
  private void addAllCandidates(HashMap<String, Member> candidates) {
    if(candidates != null) {
      for(Member candidate:candidates.values()) {
        if(candidate.state == -1) {
          candidate.state = 0;
          network.add(candidate);
        }
      }
    }
  }

  private void initialize(String name) {
    network.clear();
    
    member = p.findMember("causes");
    System.out.println("Found member:" + member);
    member.state = 0;

    network.add(member);    
  }
  
}
