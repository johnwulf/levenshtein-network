package causes;

import java.util.ArrayList;
import java.util.HashMap;

public class SyllableEntry {
  private String syllable;
  private ArrayList<HashMap<String, Member>> begin;
  private ArrayList<ArrayList<Member>> rest;
  private Member member;
  
  public SyllableEntry(String s, Member m) {
    this.syllable = s;
    member = m;
    begin = new ArrayList<HashMap<String, Member>>();
    rest = new ArrayList<ArrayList<Member>>();
  }
  
  public void addBegin(Member m) {
    HashMap<String, Member> list;

    ensureSize(begin, m.name.length());
    list = begin.get(m.name.length());
    if(list == null) {
      list = new HashMap<String, Member>();
      begin.set(m.name.length(), list);
    }

    list.put(m.name, m);
  }
  
  public void addRest(Member m) {      
    ArrayList<Member> list;

    ensureSize(rest, m.name.length());
    list = rest.get(m.name.length());
    if(list == null) {
      list = new ArrayList<Member>();
      rest.set(m.name.length(), list);
    }

    list.add(m);
  }
     
  public void setMember(Member m) {
    member = m;
    // The member also is begin and rest at the same time for its length
    addBegin(m);
    addRest(m);
  }
  
  public String getSyllable() {
    return syllable;
  }
  
  public Member getMember() {
    return member;
  }
  
  public HashMap<String, Member> getBeginMemberList(int n) {
    if(n < begin.size()) {
      return begin.get(n);
    }
    
    return null;
  }

  public ArrayList<Member> getRestMemberList(int n) {
    if(n < rest.size()) {
      return rest.get(n);
    }
    
    return null;
  }
      
  @SuppressWarnings("unchecked")
  private void ensureSize(ArrayList a, int n) {
    while(a.size() <= n) {
      a.add(null);
    }
  }
}
